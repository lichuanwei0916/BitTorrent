package com.system;

/**
 * @className: BTClient
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/13 10:13
 * @Company: Copyright [日期] by [作者或个人]
 **/

import java.io.*;
import java.net.*;
import java.util.*;

public class BTClient {
    private static final int TRACKER_PORT = 8888;
    private static final String TRACKER_ADDRESS = "localhost";
    private static final String FILE_NAME = "example.torrent";

    public static void main(String[] args) {
        try (Socket socket = new Socket(TRACKER_ADDRESS, TRACKER_PORT);
             BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             PrintWriter out = new PrintWriter(socket.getOutputStream(), true)) {

            // 发送加入请求给Tracker
            out.println("JOIN " + InetAddress.getLocalHost().getHostAddress());

            // 发送获取节点列表请求给Tracker
            out.println("GET_PEERS " + FILE_NAME);

            // 接收并处理Tracker返回的节点列表
            int numPeers = Integer.parseInt(in.readLine());
            List<String> peers = new ArrayList<>();
            for (int i = 0; i < numPeers; i++) {
                String peerAddress = in.readLine();
                peers.add(peerAddress);
            }

            // 连接到其他节点并下载文件
            for (String peer : peers) {
                downloadFileFromPeer(peer);
            }

            // 发送离开请求给Tracker
            out.println("LEAVE " + InetAddress.getLocalHost().getHostAddress());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void downloadFileFromPeer(String peerAddress) {
        try (Socket socket = new Socket(peerAddress, 12345);
             InputStream inputStream = socket.getInputStream();
             FileOutputStream fileOutputStream = new FileOutputStream(FILE_NAME)) {

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, bytesRead);
            }

            System.out.println("File downloaded from peer: " + peerAddress);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

