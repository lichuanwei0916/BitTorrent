package com.system;

/**
 * @className: BTClient1
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/13 10:45
 * @Company: Copyright [日期] by [作者或个人]
 **/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

public class BTClient11 {
    private static final String TRACKER_HOST = "localhost";//TODO: 用输入框输入TRACKER地址
    private static final int TRACKER_PORT = 8888;

    public static void main(String[] args) throws SocketException {
        // 假设要注册的资源信息
        String resourceIP = IpUtil.getLocalIp4Address() .get().toString().replaceAll("/","") + ":11234";
        String resourceName = "111.jpg";//TODO:从src/com/system下读取所有文件做成列表选择

        //TODO:按下添加资源运行下面代码
        try (Socket socket = new Socket(TRACKER_HOST, TRACKER_PORT)) {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // 发送 JOIN 消息给 Tracker
            String joinMessage = "ADD_RESOURCE " + resourceIP + " " + resourceName;
//            String joinMessage = "GET_PEERS 111.jpg";
            out.println(joinMessage);


            // 接收并处理 Tracker 的响应
            String response = in.readLine();
            System.out.println("Response from Tracker: " + response);
            // 关闭连接
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

