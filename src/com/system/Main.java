package com.system;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String directoryPath = "src/com/system/";
        File directory = new File(directoryPath);

        if (directory.exists() && directory.isDirectory()) {
            List<String> fileList = listFiles(directory);
            for (String filePath : fileList) {
                filePath = filePath.substring(directoryPath.length()).replace("\\", "/");
                System.out.println(filePath);
            }
        } else {
            System.out.println("指定目录不存在或不是一个目录");
        }
    }

    private static List<String> listFiles(File directory) {
        List<String> fileList = new ArrayList<>();

        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    fileList.add(file.getPath()); // 添加文件的完整路径
                } else if (file.isDirectory()) {
                    List<String> subList = listFiles(file); // 递归调用，遍历子目录
                    fileList.addAll(subList); // 将子目录中的文件添加到列表中
                }
            }
        }

        return fileList;
    }
}
