package com.system;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

/**
 * @className: BTClient2
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/14 8:29
 * @Company: Copyright [日期] by [作者或个人]
 **/

public class BTClient2 {
    private static final int PORT = 11234; // 发送方监听的端口号

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Sender started, listening on port " + PORT);

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("Accepted connection from " + socket.getInetAddress());
                Thread thread = new Thread(() -> {
                    String resourceName = null;
                    try (BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                         PrintWriter out = new PrintWriter(socket.getOutputStream(), true)) {
                        String request = in.readLine();
                        if(request.startsWith("HANDSHAKE")){
                            out.println("OK");
                            out.flush();
                            request = in.readLine();
                            if(request.startsWith("GET RESOURCE"))
                                resourceName = request.substring(13);
                        }
                        while ((request = in.readLine()) != null) {
                            System.out.println("接收到:" + request);
                            if (request.startsWith("REQUEST_PIECE")) {
                                String[] temp = request.substring(14).split(":");
                                int pieceIndex = Integer.parseInt(temp[0]);
                                int pieceSize = Integer.parseInt(temp[1]);
                                byte[] pieceData = getPieceData(resourceName,pieceIndex * pieceSize,pieceSize); // 获取特定索引的数据块

                                if (pieceData != null) {
//                                    out.println("PIECE_DATA"); // 响应消息，表示将发送数据块
//                                    out.flush();
                                    out.write(Arrays.toString(pieceData) + "\n");
                                    out.flush();
                                    System.out.println("Sent piece data for index " + pieceIndex);
                                } else {
//                                    out.println("PIECE_NOT_FOUND"); // 响应消息，表示未找到请求的数据块
//                                    out.flush();
                                    System.out.println("Requested piece not found for index " + pieceIndex);
                                }
                            } else {
//                                out.println("INVALID_REQUEST"); // 响应消息，表示无效的请求
//                                out.flush();
                                System.out.println("Invalid request received: " + request);
                            }
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static byte[] getPieceData(String resourceName, long offset, int length) {
        // 根据资源名称、偏移量和长度获取文件的一部分作为数据块的逻辑
        // 这里假设根据资源名称从本地存储或其他来源获取文件的指定部分的字节数组
        if (resourceName == null) {
            return null;
        }

        try {
            // 假设数据存储在以资源名称命名的文件中
            String fileName = "src/com/system/" + resourceName;
            File file = new File(fileName);

            if (file.exists()) {
                // 读取文件的指定部分内容并返回字节数组
                RandomAccessFile raf = new RandomAccessFile(file, "r");
                raf.seek(offset); // 设置文件指针偏移量
                if((offset + length) > file.length())
                    length = (int) (file.length() - offset);
                byte[] pieceData = new byte[length];
                System.out.println("length: " + length + "     offset: " + offset);
                raf.read(pieceData);
                raf.close();
                return pieceData;
            } else {
                // 文件不存在
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
