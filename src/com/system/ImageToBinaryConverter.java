package com.system;

/**
 * @className: ImageToBinaryConverter
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/14 14:53
 * @Company: Copyright [日期] by [作者或个人]
 **/

import java.io.*;

public class ImageToBinaryConverter {
    public static void main(String[] args) {
        String imageFilePath = "download/path/111.jpg";
        String binaryFilePath = "download/path/to/piece_0.dat";

        try {
            // 读取图片文件的内容
            File imageFile = new File(imageFilePath);
            FileInputStream fis = new FileInputStream(imageFile);
            byte[] imageData = new byte[(int) imageFile.length()];
            fis.read(imageData);
            fis.close();

            // 将图片数据保存为二进制文件
            FileOutputStream fos = new FileOutputStream(binaryFilePath);
            fos.write(imageData);
            fos.close();

            System.out.println("Image file converted to binary successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
