package com.system;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * @className: TrackerGUI
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/13 10:00
 * @Company: Copyright [日期] by [作者或个人]
 **/

public class TrackerGUI extends JFrame {
    private JTextArea consoleTextArea;

    public TrackerGUI() {
        setTitle("Tracker GUI");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);

        consoleTextArea = new JTextArea();
        consoleTextArea.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(consoleTextArea);
        getContentPane().add(scrollPane, BorderLayout.CENTER);
    }

    public void redirectConsoleOutput() {
        PrintStream printStream = new PrintStream(new ConsoleOutputStream(consoleTextArea));
        System.setOut(printStream);
        System.setErr(printStream);
    }

    private static class ConsoleOutputStream extends OutputStream {
        private JTextArea textArea;

        public ConsoleOutputStream(JTextArea textArea) {
            this.textArea = textArea;
        }

        @Override
        public void write(int b) throws IOException {
            SwingUtilities.invokeLater(() -> textArea.append(String.valueOf((char) b)));
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            TrackerGUI trackerGUI = new TrackerGUI();
            trackerGUI.setVisible(true);
            trackerGUI.redirectConsoleOutput();

            Tracker tracker = new Tracker();
            tracker.start();
        });
    }
}
