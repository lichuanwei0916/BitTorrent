package com.system;

/**
 * @className: TorrentInfo
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/13 14:54
 * @Company: Copyright [日期] by [作者或个人]
 **/

public class TorrentInfo {
    private String announce;
    private String name;
    private long size;
    private long piece_length;
    private String hashValue;

    public TorrentInfo() {
    }

    public String getAnnounce() {
        return announce;
    }

    public void setAnnounce(String announce) {
        this.announce = announce;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getPiece_length() {
        return piece_length;
    }

    public void setPiece_length(long piece_length) {
        this.piece_length = piece_length;
    }

    public String getHashValue() {
        return hashValue;
    }

    public void setHashValue(String hashValue) {
        this.hashValue = hashValue;
    }

    @Override
    public String toString() {
        return "Track地址=" + announce + '\n' +
                "文件名字=" + name + '\n' +
                "文件大小=" + size  +"字节"+ '\n' +
                "分块大小=" + piece_length +"字节" + '\n' +
                "哈希验证值=" + hashValue;
    }
}
