package com.system;

/**
 * @className: BTClientGUI
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/15 0:07
 * @Company: Copyright [日期] by [作者或个人]
 **/

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BTClientGUI extends JFrame {
    private static final int TRACKER_PORT = 8888;
    private static final String DIRECTORYPATH = "src/com/system/resource/";
    private JTextField trackerAddressTextField;
    private JTextField clientPortTextField;
    private JComboBox<String> resourceNameComboBox;
    private JButton selectBTButton;
    private JButton analyticaButton;
    private JButton downloadButton;
    private String BTTorrentPath;
    private JLabel responseLabel;
    private JLabel clientStatus = new JLabel("关闭中，未连接");
    private boolean isRun = false;

    public BTClientGUI() {
        setTitle("BT Client GUI");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 700);

        File directory = new File(DIRECTORYPATH);
        List<String> fileList = listFiles(directory);
        if (directory.exists() && directory.isDirectory()) {
            for (int i = 0; i < fileList.size(); i++) {
                fileList.set(i, fileList.get(i).substring(DIRECTORYPATH.length()).replace("\\", "/"));
            }
        } else {
            System.out.println("指定目录不存在或不是一个目录");
        }

        trackerAddressTextField = new JTextField();
        clientPortTextField = new JTextField();
        resourceNameComboBox = new JComboBox<>(fileList.toArray(new String[0]));
        responseLabel = new JLabel();

        JButton addButton = new JButton("Add Resource");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String trackerAddress = trackerAddressTextField.getText();
                String resourceName = (String) resourceNameComboBox.getSelectedItem();
                try {
                    String clientPort = clientPortTextField.getText();
                    int port = Integer.parseInt(clientPort);
                    addResource(trackerAddress, resourceName, port);
                } catch (NumberFormatException numE) {
                    String errorMessage = "端口号需要为整数，请重新输入";
                    JOptionPane.showMessageDialog(null, errorMessage, "错误", JOptionPane.ERROR_MESSAGE);
                } catch (SocketException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        JButton leaveButton = new JButton("Leave");
        leaveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String trackerAddress = trackerAddressTextField.getText();
                try {
                    String clientPort = clientPortTextField.getText();
                    int port = Integer.parseInt(clientPort);
                    leaveTracker(trackerAddress, port);
                } catch (SocketException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
        final Thread[] clientThread = new Thread[1];
        JButton startButton = new JButton("Start");
        JButton endButton = new JButton("End");
        JDialog outputDialog = new JDialog();
        outputDialog.setTitle("输出信息");
        outputDialog.setSize(400, 300);
        outputDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        // 创建一个文本区域用于显示输出信息
        JTextArea outputTextArea = new JTextArea();
        outputTextArea.setEditable(false);

        // 将文本区域添加到弹窗中
        outputDialog.add(new JScrollPane(outputTextArea));

        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String clientPort = clientPortTextField.getText();
                try {
                    int port = Integer.parseInt(clientPort);
                    isRun = true;
                    // 创建并显示outputDialog弹窗
                    outputDialog.setVisible(true);
                    clientThread[0] = new Thread(() -> {
                        startClient(port, outputTextArea);
                    });
                    clientThread[0].start();
                    clientStatus.setText("已启动");
                    startButton.setEnabled(false);
                    endButton.setEnabled(true);
                } catch (NumberFormatException numE) {
                    String errorMessage = "端口号需要为整数，请重新输入";
                    JOptionPane.showMessageDialog(null, errorMessage, "错误", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        endButton.setEnabled(false);
        endButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                isRun = false;
                outputDialog.setVisible(false);
                clientThread[0].stop();
                clientStatus.setText("关闭中，未连接");
                startButton.setEnabled(true);
                endButton.setEnabled(false);
            }
        });

        selectBTButton = new JButton("选择文件");
        selectBTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();

                // 设置文件选择模式为只选择文件
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

                // 创建文件过滤器
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Torrent Files", "torrent");

                // 设置文件选择器的文件过滤器
                fileChooser.setFileFilter(filter);

                // 禁用"所有文件"选项
                fileChooser.setAcceptAllFileFilterUsed(false);

                // 设置默认打开路径
                String defaultPath = "src/com/system/btTorrent";
                fileChooser.setCurrentDirectory(new File(defaultPath));

                int result = fileChooser.showOpenDialog(BTClientGUI.this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    selectBTButton.setText(selectedFile.getName());
                    BTTorrentPath = selectedFile.getAbsolutePath();
                    JOptionPane.showMessageDialog(BTClientGUI.this, "已选择文件: " + selectedFile.getAbsolutePath());
                }
            }
        });

        analyticaButton = new JButton("解析BT种子");
        analyticaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String information = torrentParser(new File(BTTorrentPath));
                    // 创建新窗口
                    JFrame frame = new JFrame("Torrent Information");
                    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    // 创建文本区域
                    JTextArea textArea = new JTextArea();
                    textArea.setText(information);
                    textArea.setEditable(false);

                    // 将文本区域添加到滚动面板中
                    JScrollPane scrollPane = new JScrollPane(textArea);

                    // 将滚动面板添加到窗口中
                    frame.getContentPane().add(scrollPane);

                    // 设置窗口大小和位置
                    frame.setSize(400, 300);
                    frame.setLocationRelativeTo(null); // 居中显示

                    // 显示窗口
                    frame.setVisible(true);
                } catch (NullPointerException nE) {
                    String errorMessage = "请选择文件后解析BT种子！";
                    JOptionPane.showMessageDialog(null, errorMessage, "错误", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        downloadButton = new JButton("下载文件");
        downloadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                class ProgressPanel extends JPanel {
                    private int[] status;

                    public void updateStatus(int[] status) {
                        this.status = status;
                    }

                    public ProgressPanel(int[] status) {
                        this.status = status;
                    }

                    @Override
                    protected void paintComponent(Graphics g) {
                        super.paintComponent(g);
                        double barWidth = (getWidth() - 20)/status.length; // 长方形的宽度
                        int barHeight = 20; // 长方形的高度

                        for (int i = 0; i < status.length; i++) {
                            double x = 10 + (i * barWidth); // 计算每个长方形的起始位置
                            int y = 10; // 长方形的纵坐标

                            if (status[i] == 0) {
                                g.setColor(Color.RED); // 设置红色
                            } else if (status[i] == 1) {
                                g.setColor(Color.GREEN); // 设置绿色
                            } else if (status[i] == 2) {
                                g.setColor(Color.YELLOW); // 设置黄色
                            } else if (status[i] == 3) {
                                g.setColor(Color.BLUE); // 设置蓝色
                            } else {
                                g.setColor(Color.pink); // 设置粉色
                            }

                            g.fillRect((int) Math.ceil(x), y, (int) Math.ceil(barWidth), barHeight); // 绘制长方形
                        }
                    }
                }

                try {
                    JFrame frame = new JFrame("下载进度");
                    frame.setSize(600, 200);
                    frame.setLayout(new BorderLayout());

                    BtDownloader btDownloader = new BtDownloader();

                    Thread downloadThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            btDownloader.downloadResource(new File(BTTorrentPath));
                        }
                    });
                    downloadThread.start();
                    while(btDownloader.getStatus() == null){

                    }
                    ProgressPanel progressPanel = new ProgressPanel(btDownloader.getStatus());

                    Timer timer = new Timer(200, null); // 初始化一个空的ActionListener

                    timer.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            int[] status = btDownloader.getStatus(); // 定时获取最新的状态数组
                            progressPanel.updateStatus(status); // 更新进度条的状态数组
                            frame.repaint(); // 重绘窗口
                            if (!downloadThread.isAlive()) {
                                timer.stop(); // 线程结束后停止计时器
                                JOptionPane.showMessageDialog(frame, "下载完成\n种子哈希值:" + btDownloader.getTorrentSHA256Hash() + "\n下载文件哈希值:" + btDownloader.getDownloadSHA256Hash()); // 增加一个弹窗提示下载完成
                            }
                        }
                    });

                    timer.start();

                    frame.add(progressPanel, BorderLayout.CENTER);
                    frame.setVisible(true);
                } catch (NullPointerException nE) {
                    String errorMessage = "请选择BT种子后下载！";
                    JOptionPane.showMessageDialog(null, errorMessage, "错误", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(12, 2));
        JLabel label = new JLabel("资源添加:");
        Font font = label.getFont();
        label.setFont(font.deriveFont(Font.BOLD, 16)); // 设置字体加粗和大小
        label.setForeground(Color.RED); // 设置字体颜色
        panel.add(label);
        panel.add(new JLabel(""));

        panel.add(new JLabel("Tracker Address:"));
        panel.add(trackerAddressTextField);

        panel.add(new JLabel("Resource Name:"));
        panel.add(resourceNameComboBox);

        panel.add(new JLabel("Client Port:"));
        panel.add(clientPortTextField);

        panel.add(addButton);
        panel.add(leaveButton);

        panel.add(new JLabel("返回消息:"));
        panel.add(responseLabel);

        label = new JLabel("服务启动:");
        font = label.getFont();
        label.setFont(font.deriveFont(Font.BOLD, 16)); // 设置字体加粗和大小
        label.setForeground(Color.RED); // 设置字体颜色
        panel.add(label);
        panel.add(new JLabel(""));

        panel.add(startButton);
        panel.add(endButton);
        panel.add(new JLabel("状态："));
        panel.add(clientStatus);

        label = new JLabel("下载文件:");
        font = label.getFont();
        label.setFont(font.deriveFont(Font.BOLD, 16)); // 设置字体加粗和大小
        label.setForeground(Color.RED); // 设置字体颜色
        panel.add(label);
        panel.add(new JLabel(""));

        panel.add(new JLabel("选择BT种子:"));
        panel.add(selectBTButton);
        panel.add(analyticaButton);
        panel.add(downloadButton);
        getContentPane().add(panel, BorderLayout.CENTER);
    }

    private void startClient(int port, JTextArea outputTextArea) {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            SwingUtilities.invokeLater(() -> {
                outputTextArea.append("Sender started, listening on port " + port + "\n");
            });

            while (isRun) {
                Socket socket = serverSocket.accept();
                SwingUtilities.invokeLater(() -> {
                    outputTextArea.append("Accepted connection from " + socket.getInetAddress() + "\n");
                });
                Thread thread = new Thread(() -> {
                    String resourceName = null;
                    try (BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                         PrintWriter out = new PrintWriter(socket.getOutputStream(), true)) {
                        String request = in.readLine();
                        if (request.startsWith("HANDSHAKE")) {
                            out.println("OK");
                            out.flush();
                            request = in.readLine();
                            if (request.startsWith("GET RESOURCE"))
                                resourceName = request.substring(13);
                        }
                        while ((request = in.readLine()) != null) {
                            String finalRequest = request;
                            SwingUtilities.invokeLater(() -> {
                                outputTextArea.append("接收到:" + finalRequest + "\n");
                            });


                            if (request.startsWith("REQUEST_PIECE")) {
                                String[] temp = request.substring(14).split(":");
                                int pieceIndex = Integer.parseInt(temp[0]);
                                int pieceSize = Integer.parseInt(temp[1]);
                                byte[] pieceData = getPieceData(resourceName, pieceIndex * pieceSize, pieceSize); // 获取特定索引的数据块

                                if (pieceData != null) {
//                                    out.println("PIECE_DATA"); // 响应消息，表示将发送数据块
//                                    out.flush();
                                    out.write(Arrays.toString(pieceData) + "\n");
                                    out.flush();
                                    SwingUtilities.invokeLater(() -> {
                                        outputTextArea.append("Sent piece data for index " + pieceIndex + "\n");
                                    });
                                } else {
//                                    out.println("PIECE_NOT_FOUND"); // 响应消息，表示未找到请求的数据块
//                                    out.flush();
                                    SwingUtilities.invokeLater(() -> {
                                        outputTextArea.append("Requested piece not found for index " + pieceIndex + "\n");
                                    });
                                }
                            } else {
//                                out.println("INVALID_REQUEST"); // 响应消息，表示无效的请求
//                                out.flush();
                                String finalRequest1 = request;
                                SwingUtilities.invokeLater(() -> {
                                    outputTextArea.append("Invalid request received: " + finalRequest1 + "\n");
                                });
                            }
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addResource(String trackerAddress, String resourceName, int port) throws SocketException {
        String resourceIP = IpUtil.getLocalIp4Address().get().toString().replaceAll("/", "") + ":" + port;
        try (Socket socket = new Socket(trackerAddress, TRACKER_PORT)) {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // 发送 ADD_RESOURCE 消息给 Tracker
            String addMessage = "ADD_RESOURCE " + resourceIP + " " + resourceName;
            out.println(addMessage);

            // 接收并处理 Tracker 的响应
            String response = in.readLine();
            responseLabel.setText("Response from Tracker: " + response);

            // 关闭连接
            socket.close();
        } catch (IOException e) {
            String errorMessage = "发生IO异常: " + e.toString();
            JOptionPane.showMessageDialog(null, errorMessage, "错误", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void leaveTracker(String trackerAddress,int port) throws SocketException {
        String resourceIP = IpUtil.getLocalIp4Address().get().toString().replaceAll("/", "") + ":" + port;
        try (Socket socket = new Socket(trackerAddress, TRACKER_PORT)) {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // 发送 ADD_RESOURCE 消息给 Tracker
            String addMessage = "LEAVE " + resourceIP;
            out.println(addMessage);

            // 接收并处理 Tracker 的响应
            String response = in.readLine();
            responseLabel.setText("Response from Tracker: " + response);

            // 关闭连接
            socket.close();
        } catch (IOException e) {
            String errorMessage = "发生IO异常: " + e.toString();
            JOptionPane.showMessageDialog(null, errorMessage, "错误", JOptionPane.ERROR_MESSAGE);
        }
    }

    private List<String> listFiles(File directory) {
        List<String> fileList = new ArrayList<>();

        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    fileList.add(file.getPath()); // 添加文件的完整路径
                } else if (file.isDirectory()) {
                    List<String> subList = listFiles(file); // 递归调用，遍历子目录
                    fileList.addAll(subList); // 将子目录中的文件添加到列表中
                }
            }
        }

        return fileList;
    }

    private byte[] getPieceData(String resourceName, long offset, int length) {
        // 根据资源名称、偏移量和长度获取文件的一部分作为数据块的逻辑
        // 这里假设根据资源名称从本地存储或其他来源获取文件的指定部分的字节数组
        if (resourceName == null) {
            return null;
        }

        try {
            // 假设数据存储在以资源名称命名的文件中
            String fileName = "src/com/system/resource/" + resourceName;
            File file = new File(fileName);

            if (file.exists()) {
                // 读取文件的指定部分内容并返回字节数组
                RandomAccessFile raf = new RandomAccessFile(file, "r");
                raf.seek(offset); // 设置文件指针偏移量
                if ((offset + length) > file.length())
                    length = (int) (file.length() - offset);
                byte[] pieceData = new byte[length];
                System.out.println("length: " + length + "     offset: " + offset);
                raf.read(pieceData);
                raf.close();
                return pieceData;
            } else {
                // 文件不存在
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String torrentParser(File torrentFile) {
        TorrentParser parser = new TorrentParser();
        try {
            TorrentInfo torrentInfo = parser.parse(torrentFile);
            return torrentInfo.toString();
        } catch (IOException e) {
            return "解析错误";
        }

    }

    class BtDownloader {
        private int[] status;//数据块状态数组
        private boolean isReceive;
        private int sum;//接收的数据块的总数
        private TorrentInfo torrentInfo;//种子信息

        public BtDownloader() {
        }

        public int[] getStatus() {
            return status;
        }

        public void setStatus(int[] status) {
            this.status = status;
        }

        public boolean isReceive() {
            return isReceive;
        }

        public void setReceive(boolean receive) {
            isReceive = receive;
        }

        public int getSum() {
            return sum;
        }

        public void setSum(int sum) {
            this.sum = sum;
        }

        public void downloadResource(File torrentFile) {
            String downloadDir = "download/path/to/";

            try {
                // 解析种子文件获取文件信息
                TorrentParser parser = new TorrentParser();
                TorrentInfo torrentInfo = parser.parse(torrentFile);
                this.torrentInfo = torrentInfo;
                downloadDir += torrentInfo.getName();
                // 创建下载目录
                File dir = new File(downloadDir);
                if (!dir.exists()) {
                    dir.mkdirs();
                }

                String resourceName = torrentInfo.getName();
                // 请求和下载数据块
                long numPieces = (long) Math.ceil((double) torrentInfo.getSize() / torrentInfo.getPiece_length());
                status = new int[(int) numPieces];
                Arrays.fill(status, 0);

                String[] announce = torrentInfo.getAnnounce().split(":");
                try (Socket socket = new Socket(announce[0], Integer.parseInt(announce[1]));
                     BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                     PrintWriter out = new PrintWriter(socket.getOutputStream(), true)) {

                    // 发送 GET_PEERS 请求获取指定资源的 PEER 列表
                    out.println("GET_PEERS " + torrentInfo.getName());

                    // 读取并解析响应
                    String response = in.readLine();
                    String temp[] = response.split(" ");
                    int numPeers = Integer.parseInt(temp[0]);
                    List<String> peers_address = new ArrayList<>();
                    List<String> peers_port = new ArrayList<>();
                    for (int i = 0; i < numPeers; i++) {
                        peers_address.add(temp[i + 1].split(":")[0]);
                        peers_port.add(temp[i + 1].split(":")[1]);
                    }

                    // 输出 PEER 列表
                    System.out.println("PEER List for resource: " + "111.jpg");
                    for (int i = 0; i < peers_address.size(); i++) {
                        System.out.println(peers_address.get(i) + ":" + peers_port.get(i));
                    }

                    socket.close();
                    in.close();
                    out.close();
                    List<Thread> threads = new ArrayList<>();
                    for (int i = 0; i < peers_address.size(); i++) {
                        String peerHost = peers_address.get(i);
                        int peerPort = Integer.parseInt(peers_port.get(i));

                        int finalI = i + 1;
                        Thread thread = new Thread(() -> {
                            try (Socket socket_peer = new Socket(peerHost, peerPort);
                                 BufferedReader in_peer = new BufferedReader(new InputStreamReader(socket_peer.getInputStream()));
                                 PrintWriter out_peer = new PrintWriter(socket_peer.getOutputStream(), true)) {

                                // 协商协议和握手
                                out_peer.println("HANDSHAKE");
                                String response_peer = in_peer.readLine();
                                if (response_peer.equals("OK")) {
                                    System.out.println("Handshake successful with " + peerHost);
                                } else {
                                    System.out.println("Handshake failed with " + peerHost);
                                    return;
                                }
                                out_peer.println("GET RESOURCE " + torrentInfo.getName());
                                for (int pieceIndex = 0; sum != numPieces; pieceIndex++) {
                                    pieceIndex = (int) (pieceIndex % numPieces);
                                    if (shouldRequestPiece(pieceIndex)) {
                                        status[pieceIndex] = finalI;
                                        out_peer.println("REQUEST_PIECE " + pieceIndex + ":" + torrentInfo.getPiece_length());
                                        out_peer.flush();
                                        byte[] pieceData = readPieceData(in_peer); // 从输入流读取数据块

                                        writePieceDataToFile(pieceData, pieceIndex, torrentInfo.getName()); // 将数据块写入本地存储
                                        System.out.println("No: " + pieceIndex + " Piece downloaded and verified successfully from " + peerHost);
                                    }
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });

                        thread.start();
                        threads.add(thread);
                    }

                    // 等待所有线程执行完毕
                    for (Thread thread : threads) {
                        try {
                            thread.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    mergeFiles(numPieces);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private boolean shouldRequestPiece(int pieceIndex) {
            if (status[pieceIndex] != 0)
                return false;
            else {
                sum++;
                status[pieceIndex] = 1;
                return true;
            }
        }

        private byte[] readPieceData(BufferedReader in) throws IOException {
            // 从输入流读取数据块
            // 这里假设数据块的长度已经在协议中进行了约定，可以直接读取指定长度的字节数据
            byte[] data;
            String byteString = in.readLine(); // 读取传输的字节数组字符串
            // 将字符串转换为字节数组
            data = parseByteArray(byteString);
            return data;
        }

        private void writePieceDataToFile(byte[] pieceData, int pieceIndex, String resourceName) throws IOException {
            // 将数据块写入本地存储
            // 这里假设将数据块写入以 pieceIndex 命名的文件中
            String fileName = "download/path/to/" + resourceName + "/" + "piece_" + pieceIndex + ".dat";
            try (FileOutputStream fos = new FileOutputStream(fileName)) {
                fos.write(pieceData);
            }
        }

        // 解析字节数组的方法
        private byte[] parseByteArray(String byteString) {
            String[] byteValues = byteString.substring(1, byteString.length() - 1).split(", ");
            byte[] byteArray = new byte[byteValues.length];
            for (int i = 0; i < byteValues.length; i++) {
                byteArray[i] = Byte.parseByte(byteValues[i]);
            }
            return byteArray;
        }

        // 合并文件
        private boolean mergeFiles(long pieceSize) {
            String outputFile = "download/" + this.torrentInfo.getName();
            try {
                FileOutputStream fos = new FileOutputStream(outputFile, true);
                FileInputStream fis;
                byte[] buffer = new byte[1024];
                for (int i = 0; i < pieceSize; i++) {
                    fis = new FileInputStream("download/path/to/" + this.torrentInfo.getName() + "/piece_" + i + ".dat");
                    int length;
                    while ((length = fis.read(buffer)) > 0) {
                        fos.write(buffer, 0, length);
                    }
                    fis.close();
                }
                fos.close();
                System.out.println("Files have been merged successfully!");
                return true;
            } catch (IOException e) {
                return false;
            }
        }

        private String getSHA256Hash(String filePath){
            try {
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                FileInputStream fis = new FileInputStream(filePath);

                byte[] dataBytes = new byte[1024];
                int bytesRead;

                while ((bytesRead = fis.read(dataBytes)) != -1) {
                    md.update(dataBytes, 0, bytesRead);
                }

                byte[] hashBytes = md.digest();

                StringBuilder sb = new StringBuilder();
                for (byte b : hashBytes) {
                    sb.append(String.format("%02x", b));
                }

                String hashValue = sb.toString();

                fis.close();
                return (hashValue);
            } catch (Exception e) {
                return null;
            }
        }

        private String getDownloadSHA256Hash(){
            return getSHA256Hash("download/" + this.torrentInfo.getName());
        }

        private String getTorrentSHA256Hash(){
            return this.torrentInfo.getHashValue();
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            BTClientGUI btClientGUI = new BTClientGUI();
            btClientGUI.setVisible(true);
        });
    }
}