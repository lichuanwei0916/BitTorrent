package com.system;

/**
 * @className: FileChooserExample
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/15 23:42
 * @Company: Copyright [日期] by [作者或个人]
 **/

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class FileChooserExample extends JFrame {
    private JButton selectButton;

    public FileChooserExample() {
        setTitle("文件选择器示例");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // 创建选择按钮
        selectButton = new JButton("选择文件");
        selectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                int result = fileChooser.showOpenDialog(FileChooserExample.this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    JOptionPane.showMessageDialog(FileChooserExample.this, "已选择文件: " + selectedFile.getAbsolutePath());
                }
            }
        });

        // 将选择按钮添加到窗口中
        getContentPane().add(selectButton);

        setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new FileChooserExample();
            }
        });
    }
}
