package com.system;

/**
 * @className: TrackerServer
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/13 8:51
 * @Company: Copyright [日期] by [作者或个人]
 **/

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tracker {
    private static final int PORT = 8888;
    private Map<String, List<String>> resourceMap;

    public Tracker() {
        resourceMap = new HashMap<>();
    }

    public void start() {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Tracker started on port " + PORT);

            while (true) {
                Socket socket = serverSocket.accept();
                handleConnection(socket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleConnection(Socket socket) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

        String message = in.readLine();
        if (message.startsWith("JOIN")) {
            String peerAddress = message.substring(5);
            addPeer(peerAddress);
            System.out.println("Peer " + peerAddress + " joined the network");
        } else if (message.startsWith("LEAVE")) {
            String peerAddress = message.substring(6);
            removePeer(peerAddress);
            System.out.println("Peer " + peerAddress + " left the network");
        } else if (message.startsWith("GET_PEERS")) {
            String resourceName = message.substring(10);
            List<String> peers = getPeers(resourceName);
            out.print(peers.size());
            for (String peer : peers) {
                out.print(" "+ peer);
            }
            out.println();
        } else if(message.startsWith("ADD_RESOURCE")){
            String resource_address = message.substring(13);
            String temp[] = resource_address.split(" ");
            addResource(temp[1],temp[0]);
            System.out.println("ADD RESOURCE:" + temp[1] + " from ip:" + temp[0]);
            out.println("OK");
            out.flush();
        }

        socket.close();
    }

    public synchronized void addPeer(String peerAddress) {
        // 将peerAddress添加到所有资源的节点列表中
        for (List<String> peerList : resourceMap.values()) {
            peerList.add(peerAddress);
        }
    }

    public synchronized void removePeer(String peerAddress) {
        // 从所有资源的节点列表中移除peerAddress
        for (List<String> peerList : resourceMap.values()) {
            peerList.remove(peerAddress.trim());
        }
        System.out.println();
    }

    public synchronized void addResource(String resourceName,String address) {
        List<String> addressList = resourceMap.computeIfAbsent(resourceName, k -> new ArrayList<>());
        // 添加一个新的资源
        if(!addressList.contains(address))
            addressList.add(address);
    }

    public synchronized List<String> getPeers(String resourceName) {
        // 获取指定资源的节点列表
        return resourceMap.getOrDefault(resourceName, new ArrayList<>());
    }

    public static void main(String[] args) {
        TrackerGUI trackerGUI = new TrackerGUI();
        trackerGUI.setVisible(true);
        trackerGUI.redirectConsoleOutput();

        Tracker tracker = new Tracker();
        tracker.start();
    }
}
