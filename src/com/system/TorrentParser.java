package com.system;

/**
 * @className: TorrentParser
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/13 14:54
 * @Company: Copyright [日期] by [作者或个人]
 **/

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TorrentParser {
    public TorrentInfo parse(File torrentFile) throws IOException {
        TorrentInfo torrentInfo = new TorrentInfo();

        try (BufferedReader reader = new BufferedReader(new FileReader(torrentFile))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("announce:")) {
                    String announce = line.substring(10).trim();
                    torrentInfo.setAnnounce(announce);
                }else if (line.startsWith("name:")) {
                    String name = line.substring(5).trim();
                    torrentInfo.setName(name);
                }
                else if (line.startsWith("size:")) {
                    long size = Long.parseLong(line.substring(5).trim());
                    torrentInfo.setSize(size);
                }
                else if (line.startsWith("piece_length:")) {
                    long piece_length = Long.parseLong(line.substring(13).trim());
                    torrentInfo.setPiece_length(piece_length);
                }
                else if(line.startsWith("hash:")){
                    String pieces = line.substring(5).trim();
                    torrentInfo.setHashValue(pieces);
                }
            }
        }

        return torrentInfo;
    }
}

