package com.system;

/**
 * @className: SHA256Hash
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/16 9:01
 * @Company: Copyright [日期] by [作者或个人]
 **/

import java.io.FileInputStream;
import java.security.MessageDigest;

public class SHA256Hash {
    public static void main(String[] args) {
        String filePath = "F:\\IDEA workspace\\BitTorrent\\src\\com\\system\\resource\\1234.mp4"; // 替换为你的文件路径

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            FileInputStream fis = new FileInputStream(filePath);

            byte[] dataBytes = new byte[1024];
            int bytesRead;

            while ((bytesRead = fis.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, bytesRead);
            }

            byte[] hashBytes = md.digest();

            StringBuilder sb = new StringBuilder();
            for (byte b : hashBytes) {
                sb.append(String.format("%02x", b));
            }

            String hashValue = sb.toString();
            System.out.println("SHA-256 Hash Value: " + hashValue);

            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

