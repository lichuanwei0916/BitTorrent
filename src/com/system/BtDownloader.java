package com.system;

/**
 * @className: BtDownloader
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/13 14:50
 * @Company: Copyright [日期] by [作者或个人]
 **/

import java.io.*;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.security.MessageDigest;

public class BtDownloader {
    private static int[] status;//数据块状态数组
    private static boolean isReceive;
    private static int sum;//接收的数据块的总数

    public static void main(String[] args) {
        String downloadDir = "download/path/to/";

        try {
            // 下载种子文件
            File torrentFile = new File("src/com/system/111.jpg.torrent");
            // 解析种子文件获取文件信息
            TorrentParser parser = new TorrentParser();
            TorrentInfo torrentInfo = parser.parse(torrentFile);

            downloadDir += torrentInfo.getName();
            // 创建下载目录
            File dir = new File(downloadDir);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            String resourceName = torrentInfo.getName();
            // 请求和下载数据块
            long numPieces = (long) Math.ceil((double) torrentInfo.getSize() / torrentInfo.getPiece_length());
            status = new int[(int) numPieces];
            Arrays.fill(status, 0);

            String[] announce = torrentInfo.getAnnounce().split(":");
            try (Socket socket = new Socket(announce[0], Integer.parseInt(announce[1]));
                 BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                 PrintWriter out = new PrintWriter(socket.getOutputStream(), true)) {

                // 发送 GET_PEERS 请求获取指定资源的 PEER 列表
                out.println("GET_PEERS " + torrentInfo.getName());

                // 读取并解析响应
                String response = in.readLine();
                String temp[] = response.split(" ");
                int numPeers = Integer.parseInt(temp[0]);
                List<String> peers_address = new ArrayList<>();
                List<String> peers_port = new ArrayList<>();
                for (int i = 0; i < numPeers; i++) {
                    peers_address.add(temp[i+1].split(":")[0]);
                    peers_port.add(temp[i+1].split(":")[1]);
                }

                // 输出 PEER 列表
                System.out.println("PEER List for resource: " + "111.jpg");
                for (int i = 0;i < peers_address.size() ;i++) {
                    System.out.println(peers_address.get(i) + ":" + peers_port.get(i));
                }

                socket.close();
                in.close();
                out.close();

                for (int i = 0; i < peers_address.size(); i++) {
                    String peerHost = peers_address.get(i);
                    int peerPort = Integer.parseInt(peers_port.get(i));

                    Thread thread = new Thread(() -> {
                        try (Socket socket_peer = new Socket(peerHost, peerPort);
                             BufferedReader in_peer = new BufferedReader(new InputStreamReader(socket_peer.getInputStream()));
                             PrintWriter out_peer = new PrintWriter(socket_peer.getOutputStream(), true)) {

                            // 协商协议和握手
                            out_peer.println("HANDSHAKE");
                            String response_peer = in_peer.readLine();
                            if (response_peer.equals("OK")) {
                                System.out.println("Handshake successful with " + peerHost);
                            } else {
                                System.out.println("Handshake failed with " + peerHost);
                                return;
                            }
                            out_peer.println("GET RESOURCE "+ torrentInfo.getName());
                            for (int pieceIndex = 0; sum!= numPieces; pieceIndex++) {
                                pieceIndex = (int) (pieceIndex % numPieces);
                                if (shouldRequestPiece(pieceIndex)) {
                                    out_peer.println("REQUEST_PIECE " + pieceIndex + ":" + torrentInfo.getPiece_length());
                                    out_peer.flush();
                                    byte[] pieceData = readPieceData(in_peer); // 从输入流读取数据块

                                    // 接收和验证数据块
//                                    boolean isValid = verifyPieceData(pieceData); // 验证数据块的完整性
//                                    if (isValid) {
                                        writePieceDataToFile(pieceData, pieceIndex, torrentInfo.getName()); // 将数据块写入本地存储
                                        System.out.println("No: " + pieceIndex + " Piece downloaded and verified successfully from " + peerHost);
//                                    } else {
//                                        System.out.println("Piece download failed or corrupted from " + peerHost);
//                                    }
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });

                    thread.start();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean shouldRequestPiece(int pieceIndex) {
        if(status[pieceIndex] == 1)
            return false;
        else{
            sum++;
            status[pieceIndex] = 1;
            return true;
        }
    }

    private static byte[] readPieceData(BufferedReader in) throws IOException {
        // 从输入流读取数据块
        // 这里假设数据块的长度已经在协议中进行了约定，可以直接读取指定长度的字节数据
        byte[] data;
        String byteString = in.readLine(); // 读取传输的字节数组字符串
        // 将字符串转换为字节数组
        data = parseByteArray(byteString);
        return data;
    }

    private static boolean verifyPieceData(byte[] pieceData) {
        // 验证数据块的完整性，可以使用哈希算法对数据块进行校验
        // 这里假设使用 SHA-256 哈希算法进行校验
//        try {
//            MessageDigest digest = MessageDigest.getInstance("SHA-256");
//            byte[] hash = digest.digest(pieceData);
//
//            // 假设有一个预先计算好的哈希值用于验证
//            byte[] expectedHash = getExpectedHash(); // 获取预期的哈希值
//
//            return Arrays.equals(hash, expectedHash);
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
        return false;
    }

    private static void writePieceDataToFile(byte[] pieceData, int pieceIndex,String resourceName) throws IOException {
        // 将数据块写入本地存储
        // 这里假设将数据块写入以 pieceIndex 命名的文件中
        String fileName = "download/path/to/" + resourceName + "/" + "piece_" + pieceIndex + ".dat";
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
            fos.write(pieceData);
        }
    }

    // 解析字节数组的方法
    private static byte[] parseByteArray(String byteString) {
        String[] byteValues = byteString.substring(1, byteString.length() - 1).split(", ");
        byte[] byteArray = new byte[byteValues.length];
        for (int i = 0; i < byteValues.length; i++) {
            byteArray[i] = Byte.parseByte(byteValues[i]);
        }
        return byteArray;
    }

}