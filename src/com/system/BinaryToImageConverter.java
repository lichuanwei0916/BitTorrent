package com.system;

/**
 * @className: BinaryToImageConverter
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/14 14:56
 * @Company: Copyright [日期] by [作者或个人]
 **/

import java.io.*;

public class BinaryToImageConverter {
    public static void main(String[] args) {
        String binaryFilePath = "download/path/to/piece_0.dat";
        String imageFilePath = "download/111.jpg";

        try {
            // 读取二进制文件的内容
            File binaryFile = new File(binaryFilePath);
            FileInputStream fis = new FileInputStream(binaryFile);
            byte[] binaryData = new byte[(int) binaryFile.length()];
            fis.read(binaryData);
            fis.close();

            // 将二进制数据保存为图片文件
            FileOutputStream fos = new FileOutputStream(imageFilePath);
            fos.write(binaryData);
            fos.close();

            System.out.println("Binary file converted to image successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

