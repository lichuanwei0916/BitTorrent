package com.system;

/**
 * @className: MergeFiles
 * @description: TODO 类描述
 * @author: Li Chuanwei
 * @date: 2023/11/14 11:02
 * @Company: Copyright [日期] by [作者或个人]
 **/

import java.io.*;

public class MergeFiles {
    public static void main(String[] args) {
        String outputFile = "download/path/111.jpg";
        try {
            FileOutputStream fos = new FileOutputStream(outputFile, true);
            FileInputStream fis;
            byte[] buffer = new byte[1024];
            for (int i = 0; i < 17; i++) {
                fis = new FileInputStream("download/path/to/111.jpg/piece_" + i + ".dat");
                int length;
                while ((length = fis.read(buffer)) > 0) {
                    fos.write(buffer, 0, length);
                }
                fis.close();
            }
            fos.close();
            System.out.println("Files have been merged successfully!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
